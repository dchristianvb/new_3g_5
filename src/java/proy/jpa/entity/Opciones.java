/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proy.jpa.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NOELIA
 */
@Entity
@Table(name = "opciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Opciones.findAll", query = "SELECT o FROM Opciones o")
    , @NamedQuery(name = "Opciones.findByIdOpciones", query = "SELECT o FROM Opciones o WHERE o.idOpciones = :idOpciones")
    , @NamedQuery(name = "Opciones.findByDescripcion", query = "SELECT o FROM Opciones o WHERE o.descripcion = :descripcion")
    , @NamedQuery(name = "Opciones.findByCorrecta", query = "SELECT o FROM Opciones o WHERE o.correcta = :correcta")})
public class Opciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idOpciones")
    private Integer idOpciones;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "Correcta")
    private String correcta;

    public Opciones() {
    }

    public Opciones(Integer idOpciones) {
        this.idOpciones = idOpciones;
    }

    public Opciones(Integer idOpciones, String descripcion, String correcta) {
        this.idOpciones = idOpciones;
        this.descripcion = descripcion;
        this.correcta = correcta;
    }

    public Integer getIdOpciones() {
        return idOpciones;
    }

    public void setIdOpciones(Integer idOpciones) {
        this.idOpciones = idOpciones;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCorrecta() {
        return correcta;
    }

    public void setCorrecta(String correcta) {
        this.correcta = correcta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOpciones != null ? idOpciones.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Opciones)) {
            return false;
        }
        Opciones other = (Opciones) object;
        if ((this.idOpciones == null && other.idOpciones != null) || (this.idOpciones != null && !this.idOpciones.equals(other.idOpciones))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "proy.jpa.entity.Opciones[ idOpciones=" + idOpciones + " ]";
    }
    
}
