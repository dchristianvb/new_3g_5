/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proy.jpa.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NOELIA
 */
@Entity
@Table(name = "tema")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tema.findAll", query = "SELECT t FROM Tema t")
    , @NamedQuery(name = "Tema.findByIdTablaTema", query = "SELECT t FROM Tema t WHERE t.idTablaTema = :idTablaTema")
    , @NamedQuery(name = "Tema.findByDescripcion", query = "SELECT t FROM Tema t WHERE t.descripcion = :descripcion")
    , @NamedQuery(name = "Tema.findByNombretema", query = "SELECT t FROM Tema t WHERE t.nombretema = :nombretema")})
public class Tema implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTabla_Tema")
    private Integer idTablaTema;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 555)
    @Column(name = "Descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Nombre_tema")
    private String nombretema;

    public Tema() {
    }

    public Tema(Integer idTablaTema) {
        this.idTablaTema = idTablaTema;
    }

    public Tema(Integer idTablaTema, String descripcion, String nombretema) {
        this.idTablaTema = idTablaTema;
        this.descripcion = descripcion;
        this.nombretema = nombretema;
    }

    public Integer getIdTablaTema() {
        return idTablaTema;
    }

    public void setIdTablaTema(Integer idTablaTema) {
        this.idTablaTema = idTablaTema;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombretema() {
        return nombretema;
    }

    public void setNombretema(String nombretema) {
        this.nombretema = nombretema;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTablaTema != null ? idTablaTema.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tema)) {
            return false;
        }
        Tema other = (Tema) object;
        if ((this.idTablaTema == null && other.idTablaTema != null) || (this.idTablaTema != null && !this.idTablaTema.equals(other.idTablaTema))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "proy.jpa.entity.Tema[ idTablaTema=" + idTablaTema + " ]";
    }
    
}
