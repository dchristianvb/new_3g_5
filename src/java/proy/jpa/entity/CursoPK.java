/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proy.jpa.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author NOELIA
 */
@Embeddable
public class CursoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "idCurso")
    private int idCurso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Estudiante_idEstudiante")
    private int estudianteidEstudiante;

    public CursoPK() {
    }

    public CursoPK(int idCurso, int estudianteidEstudiante) {
        this.idCurso = idCurso;
        this.estudianteidEstudiante = estudianteidEstudiante;
    }

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public int getEstudianteidEstudiante() {
        return estudianteidEstudiante;
    }

    public void setEstudianteidEstudiante(int estudianteidEstudiante) {
        this.estudianteidEstudiante = estudianteidEstudiante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCurso;
        hash += (int) estudianteidEstudiante;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CursoPK)) {
            return false;
        }
        CursoPK other = (CursoPK) object;
        if (this.idCurso != other.idCurso) {
            return false;
        }
        if (this.estudianteidEstudiante != other.estudianteidEstudiante) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "proy.jpa.entity.CursoPK[ idCurso=" + idCurso + ", estudianteidEstudiante=" + estudianteidEstudiante + " ]";
    }
    
}
